require "rails_helper"

RSpec.feature "Authorization errors" do
  let(:project) { FactoryGirl.create :project }

  scenario "are handle gracefully" do
    visit project_path(project)

    expect(page.current_path).to eq(root_path)
    expect(page).to have_content("You are not allowed to do that.")
  end
end